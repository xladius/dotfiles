set background=dark
colorscheme material
" set background=light
set nocompatible
let python_highlight_all=1
let g:python_highlight_all=1


" Add line numbers
set number
set cursorline
set relativenumber


hi Folded guibg=#aebbc5

set termguicolors
" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
let g:material_theme_style = 'default'
let g:material_terminal_italics = 1


set textwidth=80
set colorcolumn=+1
highlight LineNr guibg=#2c3a41
highlight GitGutterAdd guibg=#2c3a41
highlight GitGutterChange guibg=#2c3a41
highlight GitGutterChangeDelete guibg=#2c3a41
highlight GitGutterDelete guibg=#2c3a41
hi ColorColumn guibg=#2c3a41 ctermbg=246
let &colorcolumn=join(range(90,999),",")

" --------------------------------------
" Airline Config
" --------------------------------------
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'

let g:airline_theme='material'

" let g:rbpt_colorpairs = [
"     \ ['brown',       'RoyalBlue3'],
"     \ ['Darkblue',    'SeaGreen3'],
"     \ ['darkgray',    'DarkOrchid3'],
"     \ ['darkgreen',   'firebrick3'],
"     \ ['darkcyan',    'RoyalBlue3'],
"     \ ['darkred',     'SeaGreen3'],
"     \ ['darkmagenta', 'DarkOrchid3'],
"     \ ['brown',       'firebrick3'],
"     \ ['gray',        'RoyalBlue3'],
"     \ ['black',       'SeaGreen3'],
"     \ ['darkmagenta', 'DarkOrchid3'],
"     \ ['Darkblue',    'firebrick3'],
"     \ ['darkgreen',   'RoyalBlue3'],
"     \ ['darkcyan',    'SeaGreen3'],
"     \ ['darkred',     'DarkOrchid3'],
"     \ ['red',         'firebrick3'],
"     \ ]
" let g:rbpt_max = 16




"let g:rbpt_loadcmd_toggle = 0
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces




