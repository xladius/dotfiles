" +++++++++++++++++++++++++++++++++++++
" Using my vim plug to manage plugins
" +++++++++++++++++++++++++++++++++++++

call plug#begin('~/.vim/plugged')


" +++++++++++++++++++++++++++++++++++++
" Navigation / File Handling
" +++++++++++++++++++++++++++++++++++++

set rtp+=/usr/bin/fzf
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

" enhanced search feature
Plug 'wincent/loupe'

" Draging code in visual mode
Plug 'fisadev/dragvisuals.vim'

" Display vim marks on the numbers bar
" :SignatureToggle to toggle signs
Plug 'kshenoy/vim-signature'


" +++++++++++++++++++++++++++++++++++++
" Coding
" +++++++++++++++++++++++++++++++++++++

Plug 'tpope/vim-commentary' " comment/uncomment lines with gcc or gc in visual

" Switch python virtual env
" :VirtualEnvActivate <tab>
Plug 'unterzicht/vim-virtualenv'

" asyncronous linting
Plug 'w0rp/ale'
let g:ale_statusline_format = ['☀️️ %d', '🕯️ %d', '']

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

" +++++++++++++++++++++++++++++++++++++
" Code Navigation
" +++++++++++++++++++++++++++++++++++++
Plug 'brookhong/cscope.vim'
Plug 'ronakg/quickr-cscope.vim'
Plug 'hari-rangarajan/CCTree'
Plug 'valloric/youcompleteme'

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

Plug 'tmhedberg/SimpylFold'
" +++++++++++++++++++++++++++++++++++++
" git plugin
" +++++++++++++++++++++++++++++++++++++
Plug 'tpope/vim-fugitive'

" :Ghistory
Plug 'gregsexton/gitv'

" :GV to open commit browser
Plug 'junegunn/gv.vim'

Plug 'airblade/vim-gitgutter'


" +++++++++++++++++++++++++++++++++++++
" Themes and Eye Candy
" +++++++++++++++++++++++++++++++++++++

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" creates zsh theme like vim airline
" :PromptlineSnapshot ~/.shell_prompt.sh airline
" In .zshrc add: source ~/.shell_prompt.sh
Plug 'edkolev/promptline.vim'

" Themes
Plug 'chriskempson/base16-vim'
Plug 'cdmedia/itg_flat_vim'
Plug 'morhetz/gruvbox'
Plug 'https://gitlab.com/xladius/material.vim.git'

Plug 'tyrannicaltoucan/vim-quantum'
Plug 'jonathanfilip/vim-lucius'
Plug 'NLKNguyen/papercolor-theme'


" Plug 'trapd00r/vim-ansicolors'
Plug 'lilydjwg/colorizer'
" +++++++++++++++++++++++++++++++++++++
" Misc
" +++++++++++++++++++++++++++++++++++++
Plug 'ekalinin/Dockerfile.vim'
Plug 'kevinhui/vim-docker-tools'
Plug 'terryma/vim-multiple-cursors'
Plug 'mbbill/undotree'
Plug 'kien/rainbow_parentheses.vim'
Plug 'dhruvasagar/vim-table-mode'
" Markdown
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'

" vim dictionary, prosa
Plug 'reedes/vim-lexical'
Plug 'matze/vim-tex-fold'

" Plug 'justinmk/vim-sneak'

" Easy resizing
Plug 'roxma/vim-window-resize-easy'
" Alternative: /simeji/winresizer
call plug#end()
